/**
 * date: 2023/5/18
 *
 * @author Arc
 */
package com.purearc.modules.users.service.impl;

import com.purearc.modules.common.pojo.SysUser;
import com.purearc.modules.users.mapper.SysUserMapper;
import com.purearc.modules.users.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public SysUser getById(Long id) {
        return sysUserMapper.findById(id);
    }
}
