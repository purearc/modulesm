/**
 * date: 2023/5/18
 *
 * @author Arc
 */
package com.purearc.modules.users.controller;

import com.purearc.modules.common.pojo.SysUser;
import com.purearc.modules.common.util.LangsinResultUtil;
import com.purearc.modules.common.vo.LangsinResult;
import com.purearc.modules.users.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {

    @Autowired
    private SysUserService userService;

    @RequestMapping("/getById")
    public LangsinResult getById(Long id) {
        return LangsinResultUtil.success(userService.getById(id));
    }
}