package com.purearc.modules.users.service;

import com.purearc.modules.common.pojo.SysUser;

public interface SysUserService {

    SysUser getById(Long id);
}
