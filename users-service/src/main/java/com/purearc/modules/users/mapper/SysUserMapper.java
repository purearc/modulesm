package com.purearc.modules.users.mapper;

import com.purearc.modules.common.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper {
    
    SysUser findById(Long id);
}
