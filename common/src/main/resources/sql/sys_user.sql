CREATE TABLE `sys_user` (
                            `user_id` bigint NOT NULL,
                            `user_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
                            `real_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
                            `user_pass` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
                            `birthday` date DEFAULT NULL,
                            `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
                            `create_time` datetime DEFAULT NULL,
                            `update_time` datetime DEFAULT NULL,
                            PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;