CREATE TABLE `orders_info` (
                               `id` bigint NOT NULL,
                               `product_id` bigint DEFAULT NULL,
                               `product_count` bigint DEFAULT NULL,
                               `user_id` bigint DEFAULT NULL,
                               `create_time` datetime DEFAULT NULL,
                               `update_time` datetime DEFAULT NULL,
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC;