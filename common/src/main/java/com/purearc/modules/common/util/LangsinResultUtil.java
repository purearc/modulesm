package com.purearc.modules.common.util;


import com.purearc.modules.common.vo.LangsinResult;
import com.purearc.modules.common.vo.LangsinResultEnum;

/**
 * 返回数据Util
 */
public class LangsinResultUtil {

    /**
     * 根据指定的枚举，返回信息
     * @param LangsinResultEnum
     * @param object
     * @return
     */
    public static LangsinResult success(LangsinResultEnum LangsinResultEnum, Object object) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.getCode());
        LangsinResult.setMsg(LangsinResultEnum.getMsg());
        LangsinResult.setData(object);
        return LangsinResult;
    }

    /**
     * 根据指定的枚举，返回信息
     * @param LangsinResultEnum
     * @return
     */
    public static LangsinResult success(LangsinResultEnum LangsinResultEnum) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.getCode());
        LangsinResult.setMsg(LangsinResultEnum.getMsg());
        return LangsinResult;
    }

    /**
     * 成功返回的数据
     *
     * @param object
     * @return
     */
    public static LangsinResult success(String msg, Object object) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.SUCCESS_CODE.getCode());
        LangsinResult.setMsg(msg);
        LangsinResult.setData(object);
        return LangsinResult;
    }

    public static LangsinResult success(Object object) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.SUCCESS_CODE.getCode());
        LangsinResult.setMsg(LangsinResultEnum.SUCCESS_CODE.getMsg());
        LangsinResult.setData(object);
        return LangsinResult;
    }

    public static LangsinResult success() {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.SUCCESS_CODE.getCode());
        LangsinResult.setMsg(LangsinResultEnum.SUCCESS_CODE.getMsg());
        return LangsinResult;
    }

    /**
     * 失败返回数据
     *
     * @return
     */

    public static LangsinResult error(Integer code, String msg) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(code);
        LangsinResult.setMsg(msg);
        return LangsinResult;
    }

    /**
     * 根据枚举返回指定的异常信息
     * @return
     */
    public static LangsinResult error(LangsinResultEnum LangsinResultEnum) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.getCode());
        LangsinResult.setMsg(LangsinResultEnum.getMsg());
        return LangsinResult;
    }
    public static LangsinResult error(LangsinResultEnum LangsinResultEnum, String msg) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.getCode());
        LangsinResult.setMsg(msg);
        return LangsinResult;
    }
    public static LangsinResult error() {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.FAILED_CODE.getCode());
        LangsinResult.setMsg(LangsinResultEnum.FAILED_CODE.getMsg());
        return LangsinResult;
    }

    public static LangsinResult error(String msg) {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.FAILED_CODE.getCode());
        LangsinResult.setMsg(msg);
        return LangsinResult;
    }

    public static LangsinResult notLogIn() {
        LangsinResult LangsinResult = new LangsinResult();
        LangsinResult.setCode(LangsinResultEnum.NOT_LOGIN_CODE.getCode());
        LangsinResult.setMsg(LangsinResultEnum.NOT_LOGIN_CODE.getMsg());
        return LangsinResult;
    }

}
