package com.purearc.modules.common.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LangsinResult<T> {
    private Integer code;
    private String msg;
    private T data;
}
