package com.purearc.modules.common.vo;

public enum LangsinResultEnum {
    NOT_LOGIN_CODE(403, "没有登录"),
    SUCCESS_CODE(200, "成功"),
    FAILED_CODE(202, "失败");
    private Integer code;
    private String msg;

    LangsinResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
