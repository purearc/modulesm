/**
 * date: 2023/5/18
 *
 * @author Arc
 */
package com.purearc.modules.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 用户实体类
 */
@Data
@Accessors(chain = true)
public class SysUser {
    private Long userId;
    private String userName;
    private String realName;
    private String userPass;
    private Date birthday;
    private String address;
    private Date createTime;
    private Date updateTime;
}
