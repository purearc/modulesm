package com.purearc.modules.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
@Data
@Accessors(chain = true)
public class Product  {
    private Long id;

    private String productName;

    private Double price;

    private Date createTime;

    private Date updateTime;
}