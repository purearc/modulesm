/**
 * date: 2023/5/25
 *
 * @author Arc
 */
package com.purearc.modules.product.controller;

import com.purearc.modules.common.pojo.Product;
import com.purearc.modules.common.util.LangsinResultUtil;
import com.purearc.modules.common.vo.LangsinResult;
import com.purearc.modules.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @RequestMapping("/getById")
    public LangsinResult gtById(Long id) {
//        Product arrayList = productService.getById(id);
//        System.out.println(arrayList);
        return LangsinResultUtil.success(productService.getById(id));
    }
}
