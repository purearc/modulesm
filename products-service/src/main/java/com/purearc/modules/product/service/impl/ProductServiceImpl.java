/**
 * date: 2023/5/25
 *
 * @author Arc
 */
package com.purearc.modules.product.service.impl;

import com.purearc.modules.common.pojo.Product;
import com.purearc.modules.product.mapper.ProductMapper;
import com.purearc.modules.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public Product getById(Long id) {
        return productMapper.selectByPrimaryKey(id);
    }
}
