package com.purearc.modules.product.service;

import com.purearc.modules.common.pojo.Product;
import com.purearc.modules.common.vo.LangsinResultEnum;

public interface ProductService {
    Product getById(Long id);
}
