package com.purearc.modules.order.vo;

import com.purearc.modules.common.pojo.OrdersInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@Accessors(chain = true)
public class OrderInfoVo extends OrdersInfo {
    /**
     * 用户属性
     */
    private String userName;
    private String realName;
    private String userPass;
    private Date birthday;
    private String address;
    /**
     * 商品属性
     */
    private String productName;
    private Double price;
    private Double count;
}