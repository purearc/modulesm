/**
 * date: 2023/5/19
 *
 * @author Arc
 */
package com.purearc.modules.order.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.purearc.modules.common.pojo.OrdersInfo;
import com.purearc.modules.common.pojo.Product;
import com.purearc.modules.common.pojo.SysUser;
import com.purearc.modules.common.vo.LangsinResult;
import com.purearc.modules.order.mapper.OrdersInfoMapper;
import com.purearc.modules.order.service.OrderService;
import com.purearc.modules.order.vo.OrderInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrdersInfoMapper ordersInfoMapper;

    @Override
    public List<OrderInfoVo> list() {
        List<OrdersInfo> ordersInfos = ordersInfoMapper.list();
        if(CollectionUtil.isEmpty(ordersInfos)){
            return null;
        }
        List<OrderInfoVo> orderInfoVoList = new ArrayList<>();
        Map<String,Object> params = new HashMap<>();
        for (OrdersInfo ordersInfo : ordersInfos) {

            Long userId = ordersInfo.getUserId();
            //调用用户模块查询用户信息
            params.put("id",userId);
            String userInfoStr = HttpUtil.post("http://localhost:7777/userService/sysUser/getById",params);
            LangsinResult userInfo = JSONUtil.toBean(userInfoStr, LangsinResult.class);
            System.out.println(userInfo);

            Long productId = ordersInfo.getProductId();
            //调用商品模块 查询商品信息
            params.put("id",productId);
            String productInfoStr = HttpUtil.post("http://localhost:9999/productService/product/getById",params);
            LangsinResult productInfo = JSONUtil.toBean(productInfoStr, LangsinResult.class);
            System.out.println(productInfo);

            //封装成为一个新的对象OrderInfoVo
            OrderInfoVo temp = new OrderInfoVo();
            BeanUtils.copyProperties(userInfo,temp);
            BeanUtils.copyProperties(productInfo,temp);
            System.out.println(temp);
            orderInfoVoList.add(temp);
        }
        return orderInfoVoList;
    }
}
