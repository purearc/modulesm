package com.purearc.modules.order.mapper;

import com.purearc.modules.common.pojo.OrdersInfo;
import com.purearc.modules.order.vo.OrderInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrdersInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(OrdersInfo record);

    int insertSelective(OrdersInfo record);

    OrdersInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrdersInfo record);

    int updateByPrimaryKey(OrdersInfo record);

    List<OrdersInfo> list();
}
