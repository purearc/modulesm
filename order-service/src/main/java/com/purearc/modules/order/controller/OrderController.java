/**
 * date: 2023/5/19
 *
 * @author Arc
 */
package com.purearc.modules.order.controller;

import com.purearc.modules.common.pojo.OrdersInfo;
import com.purearc.modules.common.util.LangsinResultUtil;
import com.purearc.modules.common.vo.LangsinResult;
import com.purearc.modules.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping("/list")
    public LangsinResult list(){
        return LangsinResultUtil.success(orderService.list());
    }

}
