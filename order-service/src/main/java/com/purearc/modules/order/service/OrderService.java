package com.purearc.modules.order.service;

import com.purearc.modules.common.pojo.OrdersInfo;
import com.purearc.modules.order.vo.OrderInfoVo;

import java.util.List;

public interface OrderService {
    List<OrderInfoVo> list();
}
